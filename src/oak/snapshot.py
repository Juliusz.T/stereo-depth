from typing import Annotated, Literal, TypeVar

import cv2
import depthai as dai
import numpy as np
import numpy.typing as npt
from pydantic import BaseModel

DType = TypeVar("DType", bound=np.generic)
ArrayNxN = Annotated[npt.NDArray[DType], Literal["N", "N"]]
ArrayNxNx3 = Annotated[npt.NDArray[DType], Literal["N", "N", 3]]
GrayScaleImage = ArrayNxN[np.uint8]
ColorImage = ArrayNxNx3[np.uint8]
DepthMap = ArrayNxN[np.uint16]


class OakBaseModel(BaseModel):
    class Config:
        arbitrary_types_allowed = True
        validate_assignment = True
        extra = "forbid"


class Output(OakBaseModel):
    color: ColorImage
    left: GrayScaleImage
    right: GrayScaleImage
    depth: DepthMap

    def imshow(self):
        cv2.imshow("color", self.color)
        cv2.imshow("left", self.left)
        cv2.imshow("right", self.right)
        cv2.imshow("depth", self.depth)


class Snapshot(OakBaseModel):
    color: dai.ImgFrame
    left: dai.ImgFrame
    right: dai.ImgFrame
    depth: dai.ImgFrame

    def output(self) -> Output:
        return Output(
            color=self.color.getCvFrame(),
            left=self.left.getCvFrame(),
            right=self.right.getCvFrame(),
            depth=self.depth.getCvFrame(),
        )
