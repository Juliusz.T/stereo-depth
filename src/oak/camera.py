import depthai as dai
from pydantic import BaseModel

from oak.snapshot import Snapshot, OakBaseModel


class CameraConfiguration(OakBaseModel):
    framerate: int
    color_resolution: dai.ColorCameraProperties.SensorResolution = dai.ColorCameraProperties.SensorResolution.THE_12_MP
    mono_resolution: dai.ColorCameraProperties.SensorResolution = dai.MonoCameraProperties.SensorResolution.THE_800_P


class Camera:
    def __init__(
        self,
        device_info: dai.DeviceInfo,
        configuration: CameraConfiguration,
    ):
        self.device_info = device_info
        self.configuration = configuration
        self.device = dai.Device(deviceInfo=self.device_info, maxUsbSpeed=dai.UsbSpeed.SUPER)
        self._init_pipeline()

    def _init_pipeline(self):
        self.nodes: dict[str, dai.Node] = {}
        self._outputs: list[str] = ["color", "left", "right", "depth"]
        self.outputs: dict[str, dai.DataOutputQueue] = {}
        self.pipeline = dai.Pipeline()
        self._init_camera_nodes()
        self._init_depth_node()
        self._init_output_nodes()
        self.device.startPipeline(self.pipeline)
        self._init_outputs()

    def _init_camera_nodes(self) -> None:
        color_camera: dai.node.ColorCamera = self.pipeline.create(dai.node.ColorCamera)
        color_camera.setBoardSocket(dai.CameraBoardSocket.RGB)
        color_camera.setResolution(self.configuration.color_resolution)
        color_camera.setFps(self.configuration.framerate)
        color_camera.setInterleaved(False)
        self.nodes["color_camera"] = color_camera

        mono_left: dai.node.MonoCamera = self.pipeline.create(dai.node.MonoCamera)
        mono_left.setBoardSocket(dai.CameraBoardSocket.LEFT)
        mono_left.setResolution(self.configuration.mono_resolution)
        mono_left.setFps(self.configuration.framerate)
        self.nodes["left_camera"] = mono_left

        mono_right: dai.node.MonoCamera = self.pipeline.create(dai.node.MonoCamera)
        mono_right.setBoardSocket(dai.CameraBoardSocket.RIGHT)
        mono_right.setResolution(self.configuration.mono_resolution)
        mono_right.setFps(self.configuration.framerate)
        self.nodes["right_camera"] = mono_right

    def _init_depth_node(self):
        stereo_depth: dai.node.StereoDepth = self.pipeline.create(dai.node.StereoDepth)
        # stereo_depth.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_ACCURACY)
        # stereo_depth.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_3x3)
        stereo_depth.setLeftRightCheck(True)
        # stereo_depth.setSubpixel(True)
        stereo_depth.setDepthAlign(dai.CameraBoardSocket.CAM_B)
        self.nodes["left_camera"].out.link(stereo_depth.left)
        self.nodes["right_camera"].out.link(stereo_depth.right)
        self.nodes["stereo_depth"] = stereo_depth

    def _init_output_nodes(self):
        color_camera_output: dai.node.XLinkOut = self.pipeline.create(dai.node.XLinkOut)
        color_camera_output.setStreamName("color")
        self.nodes["color_camera"].isp.link(color_camera_output.input)
        self.nodes["color_output"] = color_camera_output

        mono_left_output: dai.node.XLinkOut = self.pipeline.create(dai.node.XLinkOut)
        mono_left_output.setStreamName("left")
        self.nodes["left_camera"].out.link(mono_left_output.input)
        self.nodes["left_output"] = mono_left_output

        mono_right_output: dai.node.XLinkOut = self.pipeline.create(dai.node.XLinkOut)
        mono_right_output.setStreamName("right")
        self.nodes["right_camera"].out.link(mono_right_output.input)
        self.nodes["right_output"] = mono_right_output

        stereo_depth_output: dai.node.XLinkOut = self.pipeline.create(dai.node.XLinkOut)
        stereo_depth_output.setStreamName("depth")
        self.nodes["stereo_depth"].depth.link(stereo_depth_output.input)
        self.nodes["depth_output"] = stereo_depth_output

    def _init_outputs(self):
        for key in self._outputs:
            self.outputs[key]: dai.DataOutputQueue = self.device.getOutputQueue(name=key, maxSize=1, blocking=False)

    @property
    def mxid(self) -> str:
        return self.device_info.getMxId()

    def snapshot(self) -> Snapshot:
        streams = self.outputs.keys()
        result: dict[str, dai.ImgFrame | dai.ADatatype] = {}
        for stream in streams:
            frames: list[dai.ADatatype] = self.device.getOutputQueue(stream).tryGetAll()
            result[stream] = frames[-1]
        return Snapshot(**result)
