from pathlib import Path

PACKAGE_ROOT_FOLDER = Path(__file__).parents[0]
SRC_FOLDER = PACKAGE_ROOT_FOLDER.parents[0]
GIT_REPO_FOLDER = SRC_FOLDER.parents[0]
TESTS_FOLDER = GIT_REPO_FOLDER / "test"
EXAMPLES_FOLDER = GIT_REPO_FOLDER / "examples"
