#!/usr/bin/env python3
import subprocess
import sys

import colorama
import typer

from scm.definitions import EXAMPLES_FOLDER, GIT_REPO_FOLDER, SRC_FOLDER, TESTS_FOLDER

app = typer.Typer(help="Software configuration management utils.")


class SoftwareConfigurationManager:
    def __init__(self, dry_run: False, ci: False):
        self.dry_run = dry_run
        self.ci = ci

    def execute(self, cmd, *args, **kwargs):
        def color(x):
            """Disable color outside interactive sessions"""
            if sys.stdout.isatty():
                return x
            return ""

        _cmd = [str(c) for c in cmd]
        _cmd = " ".join(_cmd)
        print(
            f"\n{color(colorama.Fore.YELLOW)}[scm] {_cmd}{color(colorama.Style.RESET_ALL)}",
            flush=True,
        )
        if not self.dry_run:
            return_code = subprocess.run(cmd, cwd=GIT_REPO_FOLDER, *args, **kwargs).returncode
            if return_code and self.ci:
                sys.exit(return_code)
            return return_code


@app.callback()
def main(
    dry_run: bool = typer.Option(False, "--dry-run", help="Print command without without executing it."),
    ci: bool = typer.Option(False, "--ci", help="Flag used on the Gitlab CI pipeline."),
):
    colorama.init()
    app.scm = SoftwareConfigurationManager(dry_run, ci)


@app.command()
def test():
    """Run pytest."""
    cmd = ["pytest", "-svx", TESTS_FOLDER]
    app.scm.execute(cmd)


@app.command()
def black():
    """Format code."""
    cmd = ["black", SRC_FOLDER, TESTS_FOLDER, EXAMPLES_FOLDER]
    if app.scm.ci:
        cmd = cmd + ["--check", "--diff"]
    app.scm.execute(cmd)


@app.command()
def isort():
    """Sort imports."""
    cmd = ["isort", SRC_FOLDER, TESTS_FOLDER, EXAMPLES_FOLDER]
    if app.scm.ci:
        cmd = cmd + ["--diff", "--check-only"]
    app.scm.execute(cmd)


@app.command()
def ruff():
    """Check code style, syntax and complexity."""
    cmd = ["ruff", "check", SRC_FOLDER, TESTS_FOLDER, EXAMPLES_FOLDER]
    app.scm.execute(cmd)


@app.command()
def style():
    """Run all static analysis tools."""
    black()
    isort()
    ruff()


@app.command()
def poetry():
    """Check consistency of poetry.lock with pyproject.toml."""
    cmd = ["poetry", "lock", "--check"]
    app.scm.execute(cmd)


@app.command()
def all():
    """Run full CI pipeline."""
    poetry()
    style()
    test()


if __name__ == "__main__":
    app()
