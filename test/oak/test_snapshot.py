import numpy as np

from oak.snapshot import Output


def test_output_deserialization():
    color_shape = (3000, 4000, 3)
    mono_shape = (800, 1280, 1)
    color = np.zeros(color_shape, dtype=np.uint8)
    left = np.zeros(mono_shape, dtype=np.uint8)
    right = np.zeros(mono_shape, dtype=np.uint8)
    depth = np.zeros(mono_shape, dtype=np.uint16)
    output = Output(color=color, left=left, right=right, depth=depth)
    np.testing.assert_equal(output.color, color)
    np.testing.assert_equal(output.left, left)
    np.testing.assert_equal(output.right, right)
    np.testing.assert_equal(output.depth, depth)
