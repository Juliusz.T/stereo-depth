import time

import cv2
import depthai as dai

from oak.camera import Camera, CameraConfiguration
from oak.snapshot import Output, Snapshot

framerate: int = 10
device_infos = dai.Device.getAllAvailableDevices()
conf = CameraConfiguration(framerate=framerate)
camera = Camera(device_info=device_infos[0], configuration=conf)

time.sleep(1)

running: bool = True
while running:
    snapshot: Snapshot = camera.snapshot()
    output: Output = snapshot.output()
    output.imshow()
    key = cv2.waitKey(1)
    if key == ord("q"):
        running = False
    time.sleep(1 / framerate)
